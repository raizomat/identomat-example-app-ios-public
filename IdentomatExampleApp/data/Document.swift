//
//  Document.swift
//  IdentomatExampleApp
//
//  Created by Identomat Inc. on 12/20/20.
//

import Foundation
class Document {
    public static let CARD : Int = 0
    public static let LICENSE : Int = 1
    public static let RESIDENCE : Int = 2
    public static let PASSPORT : Int = 3
}
