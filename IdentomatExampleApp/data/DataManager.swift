//
//  DataManager.swift
//  IdentomatExampleApp
//
//  Created by Identomat Inc. on 12/12/20.
//

import Foundation
import identomat


class DataManager{
    
    
    static var flagsDict : [String : Any] =
    [
        "language": "ka",
        "liveness": true,
        "skip_face": false,
        "skip_document": false
    ]
    static var colors : [ String : String ] = [
        "background_low" : "#011627",
        "background_high" : "#102C43",
        "shadow_color": "#011627",
        "text_color_header": "#FFFFFF",
        "text_color": "#FFFFFF",
        "primary_button": "#17BEBB",
        "primary_button_text": "#FFFFFF",
        "secondary_button": "#011627",
        "secondary_button_text": "#FFFFFF",
        "document_outer": "#17BEBB",
        "selector_header" : "#FFFFFF",
        "upload_indstructions":"#FFFFFF",
        "iteration_text" : "#FFFFFF",
        "iteration_outer" : "#011627",
    ]
    static var strings : [String : Any?] = [
        "en" : [
            "identomat_agree" : "Yes I Agree",
            "identomat_disagree": "Disagree",
        ],
        "ru" : [
            "identomat_agree": "Согласен",
            "identomat_disagree": "Не согласен",
        ],
        "es" : [
            "identomat_agree": "Acepto",
            "identomat_disagree": "No acepto",
        ],
        "ka" : [
            "identomat_agree": "ვეთანხმები",
            "identomat_disagree": "არ ვეთანხმები",
        ]
    ]


    
    static var documentTypes : [Int] = [Document.CARD, Document.LICENSE, Document.PASSPORT, Document.RESIDENCE]
    
}

