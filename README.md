Identomat Example App for iOS
===

This is an example, how to integrate identomat KYC framework into iOS app.

Setup is pretty straightforward. Installing pods and setting identomat company key
in the code is all you need.

This example works with identomat's UIKit API, although framework itself can be used
with SwiftUI too.


Happy integration!